// this is the geojson boundaries one
d3.select('body').append('p').text("malaysia's 14th general election results.").attr('id', 'derpderp');
//  color-coded in gradients by winning party and majority percentage.
let width = 1000;
let height = 500;

// dropdown list
d3.select('body').append('select').attr('id', 'dropdown')

// tooltip
let div = d3.select("body").append("div").attr("id", "tooltip").style("opacity", 0).html("derp<br>derp<br>derp");

// create the svg
let svg = d3.select('body')
  .append('svg')
  .attr('width', width)
  .attr('height', height)
  .attr('id', 'map')

let urls = ["results_winning_2018.json", "boundaries_final.json", "state.json"]
let promises = urls.map(url => d3.json(url))

Promise.all(promises).then(([results, boundaries, states]) => {
  let center = d3.geoPath().centroid(boundaries)
  let projection = d3.geoMercator().center(center).scale(Math.min(width, height) * 5.5)
  let path = d3.geoPath().projection(projection)

  svg.selectAll('path')
    .data(boundaries.features)
    .enter()
    .append('path')
    .attr('d', path)
    .classed('boundaries', 'true')
    .attr('id', function (d) { return d.properties.KodPAR; }) // use id as seat code
    // https://stackoverflow.com/questions/17452453/how-can-i-use-d3-classed-selection-to-have-name-as-function multiple classes w/ d3
    .attr('class', function (d) { return d3.select(this).attr("class") + " " + getWinner(d.properties.KodPAR); })
    .on("mouseover", function (d) {
      div.transition()
        .duration(200)
        .style("opacity", .9);
      div.html(readElectionResults(d.properties.KodPAR))

      d3.select("#" + d.properties.KodPAR).classed("hover-map", true).raise()
      // .style("left", (d3.event.pageX) + "px")
      // .style("top", (d3.event.pageY - 28) + "px");
    })
    .on("mouseout", function (d) {
      div.transition()
        .duration(500)
        .style("opacity", 0);
        d3.select("#" + d.properties.KodPAR).classed("hover-map", false).raise()
    });

  drawStates(states)

  // populate the dropdown list
  let seats = boundaries.features.map((x) => { return x.properties; })
  let select = d3.select('#dropdown').on('change', onchange)

  let options = select.selectAll('option').data(seats).enter()
    .append('option')
    .text(function (d) { return d.KodPAR + ": " + d.Parliament; })
    .attr('data-seat-id', function (d) { return d.KodPAR })// add exra info that doesn't have visual rep https://developer.mozilla.org/en-US/docs/Learn/HTML/Howto/Use_data_attributes

  function onchange() {

    let key = this[this.selectedIndex].__data__["KodPAR"]

    // need to remove any existing things with the selected-seat class
    // only transition if opacity is 0
    div.transition().duration(200).style("opacity", .9);
    div.html(readElectionResults(key))

    // add boundaries class when overriding all the classes
    // debugger;
    d3.select("#" + key).attr('class', d3.select(this).attr("class") + " " + 'selected-seat').raise() // raise means bring it to the top yay~
  }
  // functions called by the main thing
  function getWinner(seatId) {
    return seatProperties = results[seatId].winner;// https://stackoverflow.com/questions/4841254/how-to-convert-string-as-objects-field-name-in-javascript
    // initially i did results.id
  }

  function readElectionResults(seatId) {
    let seatProperties = results[seatId];

    return seatProperties.seat_name + ":<br/>Total Eligible 2018: " + seatProperties.total_eligible_2018 + "<br/>Total Voted 2018: " + seatProperties.total_voted_2018;
  }

  function drawStates(data) {
    svg.selectAll('.state')
      .data(data.features)
      .enter()
      .append('path')
      .attr('d', path)
      .attr('class', 'state')
  }
})

