#!/usr/bin/env ruby

require 'json'
require 'byebug'

def open_json(filepath)
  JSON.parse(File.read(filepath))
end

def state_boundaries(data, states)
  data['features'].select{ |x| states.include?(x['properties']['state'])}
end

def write_to_json_file(data, filename)
  File.open("#{filename}.json", 'w') { |f| f.write(data.to_json)}
end

def format_ge13_boundaries(data)
  keys = ['state', 'kodpar', 'parliament']
  # filtered = data.map{ |y| y['properties'].select{|k,v| keys.include?(k)} }
  # filtered = data.map do |x|
  #   x['properties'].select{ |k, v| keys.include?(k) }
  # end
  # byebug
  data['features'].map{ |y| y['properties'] = y['properties'].select{|k,v| keys.include?(k)} }

  data['features'].map do |x|
    x['properties']['seat_no'] = "P#{x['properties'].delete('kodpar')}"
    x['properties']['name'] = x['properties'].delete('parliament')
  end

  data
  # filtered.map do |x|
  #   x['seat_no'] = "P#{x.delete('kodpar')}"
  #   x['name'] = x.delete('parliament')
  # end

  # filtered
  # so even though it's not map! it modifies filtered itself because you do a reassignment. like you assign a new variable to x['seat_no']. and because the last thing you return from the map is x.delete('parliament') so that's how you end up with an array of all the parliment values (which are seat names). so like ['sibu', 'tenom', etc]
  # check that this returns a proper mapped thing and not just x['name']
end

EAST_MSIA_STATES = ["SARAWAK", "SABAH", "W.P LABUAN"]

ge13_boundaries = open_json('ge13.json')
ge13_boundaries['features'] = state_boundaries(ge13_boundaries, EAST_MSIA_STATES)
formatted_ge13_boundaries = format_ge13_boundaries(ge13_boundaries)
write_to_json_file(formatted_ge13_boundaries, "eastMsia#{Time.now.to_i}")

def format_ge14_boundaries(data)
  data['features'].map do |x|
    x['properties']['seat_no'] = format_seat_numbers(x['properties']['seat_no'])
  end

  data
end

def format_seat_numbers(str)
  "P" + str.to_i.to_s
end

# format GE14 boundaries
ge14_boundaries = open_json('ge14.json')
# byebug
formatted_ge14_boundaries = format_ge14_boundaries(ge14_boundaries)

# combine GE13 (East Msia data) + GE14 (Peninsular Msia data)
formatted_ge14_boundaries['features'] = formatted_ge14_boundaries['features'] + formatted_ge13_boundaries['features']

write_to_json_file(formatted_ge14_boundaries, "ge14_#{Time.now.to_i}")
