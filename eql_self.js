

// var g = svg.append('g')

let prom1 = d3.csv("results.csv")
let prom2 = d3.json("parliament_ge14.json")

Promise.all([prom1, prom2])
  .then(([results, boundaries]) => {
    let res = results.reduce( (acc, seat) => {
      acc[seat.seat_code] = seat;
      return acc;
    }, {})

    // let temp = d3.select("#test")
    let temp=d3.select("#test2") // for east msia
    .selectAll('polygon') // for our svg
    // .selectAll('use') // for the wiki one
    .each(function(d) { return d})

    .attr('fill', function() { return color(this, res) })
    // .on("mouseover", function(d) {
    //   div.transition()
    //       .duration(200)
    //       .style("opacity", .9);
    //   div.html(readElectionResults(this, res))
    // })
    // .attr('stroke', 'black')
    // console.log(temp)
    // .each(function (d) { console.log (d)})
    // .attr('fill', function(d) { return color(d) })
    // temp.map
    // debugger
  })


  function color(polygon, results){
    if (['seat', 'legend'].includes(polygon.id)) { return}

    let id = formatSeatId(polygon.id)
    let seatProperties = results[id];
    // debugger;
    if (seatProperties.BN_winlose_2018 === '1') {
      return 'dodgerblue';
    } else if (seatProperties.Pakatan_winlose_2018 === '1') {
      return 'darkred';
    } else if (seatProperties.PAS_winlose_2018 === '1') {
      return 'green';
    } else if (seatProperties.Other_2018 === '1') {
      return 'purple'; // independent or other party
    } else {
      console.log(seatProperties)
      // the only one is P180 kitingan
      return 'yellow'; // you should not go here!!!
    }
  }

  function formatSeatId(id) {
    // return "P" + parseInt(id); // for geojson

    return "P" + parseInt(id.match(/\d+/)) // for wiki
  }
