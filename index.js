// this is the one with the geojsons
d3.select('body').append('p').text("malaysia's 14th general election results.").attr('id', 'derpderp');
//  color-coded in gradients by winning party and majority percentage.
let width = 1000;
let height = 500;

// create the svg
let svg = d3.select('body')
  .append('svg')
  .attr('width', width)
  .attr('height', height)
  .attr('id', 'map')
// .attr('stroke', 'black')
// .attr('stroke-width', 1)

// tooltip
var div = d3.select("body")
  .append("div")
  .attr("class", "tooltip")
  .style("opacity", 0);

// load the json
d3.json('parliament_ge14.json').then(function (ge14) {
  // set the projection
  let center = d3.geoPath().centroid(ge14)
  console.log(center)
  let projection = d3.geoMercator().center(center).scale(Math.min(width, height) * 5)
  let path = d3.geoPath().projection(projection)

  svg.selectAll('path')
    .data(ge14.features)
    .enter()
    .append('path')
    .attr('d', path)
    .attr('id', function (d) { return d.properties.KodPAR }) // use id as seat code
    .attr('class', 'test2')
    .style('fill', function (d) { return colorState(d.properties.State) })
    //  .style('fill', function(d) { return checkWinningParty(d.properties) })
    .attr('stroke', 'black')
    .attr('stroke-width', 1)
    // probably can make the animations a separate function
    .on("mouseover", function (d) {
      div.transition()
        .duration(200)
        .style("opacity", .9);
      div.html(d.properties.Parliament + "<br/>")
      // .style("left", (d3.event.pageX) + "px")
      // .style("top", (d3.event.pageY - 28) + "px");
    })
    .on("mouseout", function (d) {
      div.transition()
        .duration(500)
        .style("opacity", 0);
    });

})


// color by majority later
function checkWinningParty(seatProperties) {
  if (seatProperties.BN_winlose_2018 === '1') {
    return 'steelblue';
  } else if (seatProperties.Pakatan_winlose_2018 === '1') {
    return 'burgundy';
  } else if (seatProperties.PAS_winlose_2018 === '1') {
    return 'green';
  } else if (seatProperties.Other_2018 === '1') {
    return 'purple'; // independent or other party
  } else {
    return 'yellow'; // you should not go here!!!
  }
}

function colorState(state) {
  // console.log(state
  if (state.length < 6) {
    console.log(state)
    return "steelblue";
  }
  else if (state.length < 9) {
    return "green";
  } else {
    return 'salmon';
  }
}

