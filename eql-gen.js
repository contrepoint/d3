// equal area cartogram, all programatically generated
// paths from `d3.selectAll('.seat').each(function() { x[this.id] = d3.select(this).attr('d') })`

// not working for now text not showing up
// let colorToggler = d3.select('body').append('form').attr('id', 'color-toggler')

// colorToggler.selectAll('input').data(['Default', 'Colorblind']).enter().append('input').attr('type', 'radio').attr('name', 'coloursoftheworld').attr('value', 'test').html('test')

// todo: try to keep the selected seat when toggling between colourblind and not colourblind? not urgent though

let urls = ["results_winning_2018.json", "path_and_seat2.json", "sinar_final.json"]
let promises = urls.map(url => d3.json(url))

Promise.all(promises).then(([results, paths, vote_counts]) => {
  let main = d3.select('#main')
  setupDropdownList()
  let group = setupSvg()
  drawSeats()
  drawStates()

  setupBarChart()
  drawLegend()

  function drawLegend() {
    group.selectAll('.legend').data(paths.legends).enter()
      .append('path')
      .attr('d', d => d.path)
      .attr('class', d => 'legend ' + d.party)
      .attr('data-party', d => d.party)
    // fiddle with text placement later
    group.selectAll('.word').data(paths.legends).enter().append('text')
      .attr('x', d => d.x_coordinate)
      .attr('y', d => d.y_coordinate)
      .html(d => d.display_text)
    // .attr('class', 'word')

    group.selectAll('.title').data(paths.title).enter().append('text')
      .attr('x', d => d.x_coordinate)
      .attr('y', d => d.y_coordinate)
      .html(d => d.display_text)
      .attr('class', 'title')
      .attr('xml:space', 'preserve')
  }

  let tooltip = drawTooltip()

  drawColorToggler()

  d3.selectAll("input[name='coloursoftheworld']")
    .on("change", changeColors)

  // main functions~
  function setupDropdownList() {
    main.append('select').attr('id', 'eq-dropdown')
      .selectAll('option')
      .data(paths.seats).enter()
      .append('option')
      .text(d => d.seat_name)
      .attr('data-test', d => d.seat_id)

    d3.select('#eq-dropdown').on('change', seatSelected)

    function seatSelected() {
      let key = this[this.selectedIndex].dataset.test;

      tooltip.html(readElectionResults(key))

      removeExistingSelected()

      d3.select('#' + key).classed('selected', true).raise()
    }
  }

  function setupBarChart() {
    let div = d3.select('#main').append('div').attr('id', 'bar-chart');
  }

  function drawTooltip() {
    let defaultSeatId = 'P1';

    return main.append("div").attr("id", "tooltip").html(readElectionResults(defaultSeatId));
  }

  function setupSvg() {
    let width = 600.0;
    let height = 320;
    let svg = main.append('svg').attr('id', 'map')

    if ((window.innerWidth) < width) {
      let scale = window.innerWidth / width * 0.95;
      svg.attr('width', scale * width)
        .attr('height', scale * height)

      return svg.append('g').attr('id', 'svg-group')
        .style('transform', function (d) { return getScale(scale) })

      function getScale(scale) {
        return "scale(" + String(scale) + "," + String(scale) + ")";
      }

    } else {
      svg.attr('width', width).attr('height', height)

      return svg.append('g').attr('id', 'svg-group')
    }
  }

  function drawSeats() {
    group.selectAll('path')
      .data(paths.seats)
      .enter()
      .append('path')
      .attr('d', d => d.path)
      .attr('id', d => d.seat_id)
      .attr('class', d => "seat " + getWinner(d.seat_id))
      .attr('data-winner', d => getWinner(d.seat_id))
      .on('mouseover', function (d) {
        let id = d.seat_id;
        showResults(id)
        removeExistingSelected()

        d3.select('[data-test=' + id + ']')._groups[0][0].selected = true
        d3.select("#" + id).classed('selected', true).raise()
      })

    markDefaultSeat()

    function markDefaultSeat() {
      let defaultSeatId = 'P1';

      d3.select('#' + defaultSeatId).classed('selected', true).raise()
    }
  }

  function drawStates() {
    group.selectAll('.state')
      .data(paths.states)
      .enter()
      .append('path')
      .attr('d', d => d['path'])
      .attr('class', 'state')
  }

  function drawColorToggler() {
    let colorToggler = main.append('div').attr('id', 'color-toggler')
    // https://bl.ocks.org/Jverma/2385cb7794d18c51e3ab
    // let colorToggler = group.append('div').attr('x', 300).attr('y', 300).attr('id', 'toggle')

    colorToggler.html("<form action=''><input type='radio' class='color-radio' name='coloursoftheworld' value='default' checked>Default<br><input type='radio' class='color-radio' name='coloursoftheworld' value='colourblind'>Colourblind<br></form>")
  }

  function changeColors() {
    if (this.value === 'colourblind') {
      d3.selectAll('.seat')
        .attr('class', function (d) {
          return "seat " + this.dataset.winner + "-colourblind"
        })

      d3.selectAll('.legend')
        .attr('class', function (d) { return "legend " + this.dataset.party + "-colourblind" })

      d3.selectAll('.chart')
        .attr('class', function () { return 'chart ' + this.dataset.party + '-colourblind'; })

    } else if (this.value === 'default') {
      d3.selectAll('.seat')
        .attr('class', function (d) { return "seat " + this.dataset.winner })

      d3.selectAll('.legend')
        .attr('class', function () { return "legend " + this.dataset.party })

      d3.selectAll('.chart')
        .attr('class', function () { return 'chart ' + this.dataset.party; })
    }
  }

  // functions called by multiple other functions
  function removeExistingSelected() {
    // remove any existing seats that are selected
    d3.select('.selected').classed('selected', false).lower()
  }

  function showResults(id) {
    tooltip.html(readElectionResults(id))
  }

  function readElectionResults(seatId) {
    let seatProperties = results[seatId];
    let votes = vote_counts[seatId];

    // remove earlier divs
    d3.select("#bar-chart").selectAll("*").remove();

    function getMax(arr) {
      let allVotes = arr.map(x => x['Votes for Candidate']);
      return d3.max(allVotes);
    }

    let max = 0;
    max = getMax(votes)

    d3.select('#bar-chart').selectAll('div').data(votes).enter().append('div')
      .style('width', function (d) {
        return getWidth(max, d['Votes for Candidate']) + "px";
      })
      .text(function (d) { return d.Coalition })
      .attr('class', d => d.Coalition + '-chart chart')
      .attr('data-party', function (d) { return d.Coalition + '-chart' })

    // keep at 6 lines. max # of candidates
    let additionalDivs = 6 - votes.length;
    for (var i = 0; i < additionalDivs; i++) {
      d3.select('#bar-chart').append('div').html('<br>');
    }

    function getWidth(max, votes) {
      let scale = d3.scaleLinear().domain([0, max]).range([0, 200])
      return scale(votes)
    }

    return seatProperties.seat_name + ":<br/>Total Eligible 2018: " + seatProperties.total_eligible_2018 + "<br/>Total Voted 2018: " + seatProperties.total_voted_2018;
  }

  function getWinner(seatId) {
    return results[seatId].winner;
  }
})
