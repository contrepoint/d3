// probably store the winning party in a data attribute. easier that way?
let urls = ["results_winning_2018.json", "boundaries_final.json", "state.json"]
let promises = urls.map(url => d3.json(url))

let div = d3.select("body").append("div").attr("id", "tooltip").style("opacity", 0).html("derp<br>derp<br>derp");

d3.select('body').append('select').attr('id', 'eq-dropdown')


Promise.all(promises).then(([results, boundaries, states]) => {
  let temp = d3.select("#derp")
    .selectAll('.seat')
    .each(function (d) { return this; })
    .attr('class', function (d) {
      return "seat " + getWinner(this)
      // hacky for now later get classes programmatically
    })
    .on('mouseover', function (d) {
      showResults(formatSeatId(this.id))
      d3.select("#" + this.id).classed("hover-eq", true).raise()
    })
    .on("mouseout", function (d) {
      removeDiv();
      d3.select("#" + this.id).classed("hover-eq", false)
    })

  // populate the dropdown list
  let seats = Array.from(d3.selectAll('.seat')._groups[0]).map(x => x.id); //officially the hackiest thing ever
  let selection = d3.select('#eq-dropdown').on('change', onchange)
  let options = selection.selectAll('option').data(seats).enter().append('option').text(function (d) { return d })
  // show after you've formatted it
  d3.select("#derp").style('opacity', '1')

  function onchange() {
    let key = formatSeatId(this[this.selectedIndex].__data__)

    div.transition().duration(200).style("opacity", .9);
    div.html(readElectionResults(key))

  }
  function showResults(id) {
    div.html(readElectionResults(id))
    div.transition().duration(200).style("opacity", 1);
  }

  function removeDiv() {
    div.transition()
      .duration(200)
      .style("opacity", 0);
  }
  function readElectionResults(seatId) {
    let seatProperties = results[seatId];

    return seatProperties.seat_name + ":<br/>Total Eligible 2018: " + seatProperties.total_eligible_2018 + "<br/>Total Voted 2018: " + seatProperties.total_voted_2018;
  }

  function getWinner(polygon) {
    if (!polygon.id.startsWith("P")) { return 'blank' }


    let seatId = formatSeatId(polygon.id);
    return results[seatId].winner;
  }

  function formatSeatId(id) {
    return "P" + parseInt(id.match(/\d+/)) // for wiki
  }

  // some radio button experimentation
  d3.selectAll("input[name='coloursoftheworld']").on("change", function () {
    if (this.value === 'colourblind') {
      d3.selectAll('.seat')
        .each(function (d) { return this; })
        .attr('class', function (d) {
          return "seat " + getWinner(this) + "-colorblind"
        })

      d3.selectAll('.legend')
        .each(function (d) { return this; })
        .attr('class', function (d) { return "legend " + this.dataset.party + "-colorblind" })

    } else if (this.value === 'default') {
      d3.selectAll('.seat')
        .each(function (d) { return this; })
        .attr('class', function (d) {
          return "seat " + getWinner(this)
        })

        d3.selectAll('.legend')
        .each(function (d) { return this; })
        .attr('class', function (d) { return "legend " + this.dataset.party })
    }
  });
})
